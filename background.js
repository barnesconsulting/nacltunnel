function onLaunched(launchData) {
	chrome.app.window.create('index.html', {
		width: 512,
		height: 400,
		frame: 'none'
	});
}

chrome.app.runtime.onLaunched.addListener(onLaunched);
