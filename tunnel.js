// Copyright (c) 2013 T. Scott Barnes.  Based on NaCl 'socket' example.

// Copyright (c) 2013 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

var defaults = {
	'port': '8080',
	'remoteHost': '216.97.58.27:7443' // '69.13.107.204:443'
}

if(chrome && chrome.storage) {
	chrome.storage.local.get(defaults, function(items) {
		for( var key in items) {
			var element = document.getElementById(key);
			if(element) {
				element.value = items[key];
			}
		}
	});

	chrome.storage.onChanged.addListener(function(changes, areaName) {
		if(areaName == 'local') {
			for( var key in changes) {
				var element = document.getElementById(key);
				if(element) {
					element.value = changes[key].newValue;
				}
			}
		}
	});
} else {
	document.addEventListener('DOMContentLoaded', function(event) {
		for( var key in defaults) {
			var element = document.getElementById(key);
			if(element) {
				element.value = defaults[key];
			}
		}
	});
}

// Called by the common.js module.
function attachListeners() {
	document.getElementById('listen').addEventListener('click', doListen);
	document.getElementById('setRemoteHost').addEventListener('click',
			setRemoteHost);
	if(chrome && chrome.app && chrome.app.window) {
		document.getElementById('maximize')
				.addEventListener('click', maxWindow);
		document.getElementById('close').addEventListener('click', closeWindow);
	} else {
		var windowControls = document.getElementsByTagName('window-controls')[0];
		document.body.removeChild(windowControls);
	}
}

// Called by the common.js module.
function moduleDidLoad() {
	// The module is not hidden by default so we can easily see if the plugin
	// failed to load.
	common.hideModule();
}

var msgSetRemoteHost = 'h;';
var msgSetUseFilter = 'f;';
var msgClose = 'c;';
var msgListen = 'l;';

function setRemoteHost(event) {
	event.preventDefault();
	var host = document.getElementById('remoteHost').value;
	var ssl = document.getElementById('remoteHostSSL').checked;
	var use_filter = document.getElementById('useFilter').checked;
	common.naclModule.postMessage(msgSetUseFilter + (use_filter ? 'y' : 'n'));
	common.naclModule.postMessage(msgSetRemoteHost + (ssl ? 's;' : 'x;') + host);
	if(chrome && chrome.storage) {
		chrome.storage.local.set({
			'remoteHost': host,
			'remoteHostSSL': ssl,
			'useFilter': use_filter
		});
	}
}

function doListen(event) {
	// Listen a the given port.
	event.preventDefault();
	var port = document.getElementById('port').value;
	common.naclModule.postMessage(msgListen + port);
	if(chrome && chrome.storage) {
		chrome.storage.local.set({
			'port': port
		});
	}
}

function doClose() {
	// Send a request message. See also socket.cc for the request format.
	common.naclModule.postMessage(msgClose);
}

function handleMessage(message) {
	common.logMessage(message.data);
}

function maxWindow(event) {
	chrome.app.window.current().maximize();
}

function closeWindow(event) {
	chrome.app.window.current().close();
}