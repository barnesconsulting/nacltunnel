// Copyright (c) 2013 T. Scott Barnes.

#include "ppapi/cpp/module.h"
#include "ppapi/cpp/instance.h"
#include "nacl_io/nacl_io.h"
#include <openssl/ssl.h>

#include "tunnel_instance.h"

// The TunnelModule provides an implementation of pp::Module that creates
// TunnelInstance objects when invoked.
class TunnelModule: public pp::Module {
public:
	TunnelModule() :
			pp::Module() {
	}
	virtual ~TunnelModule() {
	}

	virtual pp::Instance* CreateInstance(PP_Instance instance) {
		nacl_io_init_ppapi(instance, this->Get()->get_browser_interface());
		return new TunnelInstance(instance);
	}
};

// Implement the required pp::CreateModule function that creates our specific
// kind of Module.
namespace pp {
Module* CreateModule() {
    ERR_load_BIO_strings();
    SSL_load_error_strings();
    SSL_library_init();
	return new TunnelModule();
}
}  // namespace pp
