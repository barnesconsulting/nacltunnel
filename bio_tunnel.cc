/*
 * bio_tunnel.cpp
 *
 *  Created on: Apr 25, 2014
 *      Author: scott
 */

#include <string>
#include <sstream>
#include <stdlib.h>
#include <openssl/ssl.h>
#include <ppapi/c/pp_errors.h>
#include <ppapi/cpp/instance.h>
#include <ppapi/cpp/message_loop.h>
#include <ppapi/cpp/tcp_socket.h>
#include <ppapi/cpp/net_address.h>
#include <ppapi/cpp/host_resolver.h>
#include <ppapi/cpp/completion_callback.h>
#include <ppapi/utility/completion_callback_factory.h>
#include "bio_tunnel.h"

BIOTunnel::BIOTunnel(pp::Instance &pp_instance) :
		callback_factory(this),
		instance(pp_instance),
		ssl_connected(false),
		filter_data(false) {
	resolver = pp::HostResolver(&instance);
	local_socket = pp::TCPSocket();
	remote_socket = pp::TCPSocket(&instance);
	bio_local = NULL;
	bio_remote = NULL;
	bio_ssl_local = NULL;
	ssl = NULL;
	ctx = NULL;
}

BIOTunnel::~BIOTunnel() {
	this->Close();
	BIO_free(bio_remote);
	BIO_free(bio_local);
	BIO_free(bio_ssl_local);
	SSL_CTX_free(ctx);
}

void BIOTunnel::Connect(const pp::TCPSocket &new_local_socket,
	const std::string &new_remote_host, bool use_ssl, bool use_filter) {
	local_socket = new_local_socket;
	remote_host = new_remote_host;
	filter_data = use_filter;

	uint16_t port = (use_ssl ? 443 : 80);
	std::string hostname = remote_host;
	size_t pos = remote_host.rfind(':');
	if (pos != std::string::npos) {
		hostname = remote_host.substr(0, pos);
		port = atoi(remote_host.substr(pos + 1).c_str());
	}

	if (use_ssl) {
		ctx = SSL_CTX_new(SSLv23_client_method());
		BIO_new_bio_pair(&bio_ssl_local, 0, &bio_remote, 0);
	} else {
		BIO_new_bio_pair(&bio_local, 0, &bio_remote, 0);
	}

	pp::CompletionCallback callback = callback_factory.NewCallback(
		&BIOTunnel::OnResolve);
	PP_HostResolver_Hint hint = { PP_NETADDRESS_FAMILY_UNSPECIFIED, 0 };
	resolver.Resolve(hostname.c_str(), port, hint, callback);
}

void BIOTunnel::Close() {
	remote_socket.Close();
	local_socket.Close();

	remote_socket = pp::TCPSocket();
	local_socket = pp::TCPSocket();
}

void BIOTunnel::MessageLoopWorker(int32_t status) {
	std::ostringstream message;
	if (status != PP_OK) {
		message << "tunnel: Message loop worker failed:" << status;
		instance.PostMessage(message.str());
		return;
	}

	if (remote_socket.is_null()) {
		delete this;
		return;
	}

	if (BIO_pending(bio_remote) > 0) {
		char buffer[BIO_TUNNEL_BUFFER_SIZE];
		int32_t bytes = BIO_read(bio_remote, buffer, BIO_TUNNEL_BUFFER_SIZE);
		pp::CompletionCallback callback = callback_factory.NewCallback(
			&BIOTunnel::OnRemoteWrite);
		remote_socket.Write(buffer, bytes, callback);
	}
	if (!ssl_connected) {
		int32_t result = BIO_do_handshake(bio_local);
		if (result == 1) {
			message << "tunnel: SSL handshake succeeded.";
			instance.PostMessage(message.str());
			ssl_connected = true;
			pp::CompletionCallback local_callback =
				callback_factory.NewCallback(&BIOTunnel::OnLocalRead);
			local_socket.Read(local_read_buffer,
				BIO_get_write_guarantee(bio_local), local_callback);
		} else {
			if (!BIO_should_retry(bio_local)) {
				message << "tunnel: SSL handshake failed:" << result;
				instance.PostMessage(message.str());
				delete this;
				return;
			} else {
				pp::CompletionCallback remote_callback =
					callback_factory.NewCallback(&BIOTunnel::OnRemoteRead);
				remote_socket.Read(remote_read_buffer,
					BIO_get_write_guarantee(bio_remote), remote_callback);
			}
		}
	}
	if (!ctx || ssl_connected) {
		char buffer[BIO_TUNNEL_BUFFER_SIZE];
		int32_t bytes = BIO_read(bio_local, buffer, BIO_TUNNEL_BUFFER_SIZE);
		if (bytes > 0) {
			pp::CompletionCallback callback = callback_factory.NewCallback(
				&BIOTunnel::OnLocalWrite);
			local_socket.Write(buffer, bytes, callback);
		} else {
			pp::CompletionCallback remote_callback =
				callback_factory.NewCallback(&BIOTunnel::OnRemoteRead);
			remote_socket.Read(remote_read_buffer,
				BIO_get_write_guarantee(bio_remote), remote_callback);
		}
	}

	pp::CompletionCallback callback = callback_factory.NewCallback(
		&BIOTunnel::MessageLoopWorker);
	pp::MessageLoop::GetCurrent().PostWork(callback, 50);
}

int32_t BIOTunnel::FilterData(int32_t bytes) {
	char host[99] = "Host: new.host.com\r\n";

	char *p1 = NULL;
	char *p2 = NULL;
	char *pIn = local_read_buffer;
	char *pOut = local_filter_buffer;
	int len = 0;

	//memcpy(pOut, pIn, inBytes); return inBytes;

	/* Replace " http://" to " /_http_/" */
	p1 = strstr(pIn, " http://");

	/* Do not replace Referer: */
	if (p1 && *(p1 - 1) != ':') {

		/* Copy from pIn to p1 */
		len = p1 - pIn;
		memcpy(pOut, pIn, len);
		pOut += len;
		pIn = p1 + strlen(" http://");

		/* Skip " http://" */
		strcpy(pOut, " /_http_/");
		pOut += strlen(" /_http_/");

		/* Search the end of this field */
		p2 = strstr(pIn, "\r\n");
		if (p2 == NULL) {
			/* This may be the case that we don't have enough data in
			 * the buffer - we don't handle this case for now
			 */
			return PP_ERROR_INPROGRESS;
		}

		/* Copy from Pin to end */
		len = p2 + strlen("\r\n") - pIn;
		memcpy(pOut, pIn, len);
		pOut += len;
		pIn += len;

		/* Insert Host header */
		strcpy(pOut, host);
		pOut += strlen(host);
	}

	/* remove existing Host header if any */
	p1 = strstr(pIn, "Host: ");
	if (p1 == NULL) {
		/* No host header, copy the rest and return */
		len = bytes - (pIn - local_read_buffer);
		memcpy(pOut, pIn, len);
		pOut += len;
		pIn += len;
		return pOut - local_filter_buffer;
	}

	/* search the end of Host header */
	p2 = strstr(p1, "\r\n");
	if (p2 == NULL) {
		/* This may be the case that we don't have enough data in the buffer
		 * we don't handle this case for now
		 */
		return PP_ERROR_INPROGRESS;
	}

	/* Copy from pIn to p1 */
	len = p1 - pIn;
	memcpy(pOut, pIn, len);
	pOut += len;
	pIn += len;

	pIn = p2 + strlen("\r\n");

	/* Copy from pIn to end */
	len = bytes - (pIn - local_read_buffer);
	memcpy(pOut, pIn, len);
	pOut += len;
	pIn += len;

	return pOut - local_filter_buffer;
}

void BIOTunnel::OnResolve(int32_t result) {
	std::ostringstream message;
	if (result != PP_OK) {
		message << "remote: Resolve failed: " << result;
		instance.PostMessage(message.str());
		delete this;
		return;
	}

	remote_address = resolver.GetNetAddress(0);
	std::string addr_str = remote_address.DescribeAsString(true).AsString();
	message << "remote: Resolved: " << addr_str;
	instance.PostMessage(message.str());
	instance.PostMessage("remote: Connecting...");

	pp::CompletionCallback callback = callback_factory.NewCallback(
		&BIOTunnel::OnConnect);
	remote_socket.Connect(remote_address, callback);
}

void BIOTunnel::OnConnect(int32_t result) {
	std::ostringstream message;
	if (result != PP_OK) {
		message << "remote: Connecting socket failed: " << result;
		instance.PostMessage(message.str());
		delete this;
		return;
	}

	message << "remote: Connected socket: " << result;
	instance.PostMessage(message.str());

	if (ctx) {
		bio_local = BIO_new_ssl(ctx, 1);
		BIO_push(bio_local, bio_ssl_local);
		BIO_get_ssl(bio_local, &ssl);
	} else {
		pp::CompletionCallback local_callback = callback_factory.NewCallback(
			&BIOTunnel::OnLocalRead);
		local_socket.Read(local_read_buffer, BIO_get_write_guarantee(bio_local),
			local_callback);
	}

	pp::CompletionCallback remote_callback = callback_factory.NewCallback(
		&BIOTunnel::OnRemoteRead);
	remote_socket.Read(remote_read_buffer, BIO_get_write_guarantee(bio_remote),
		remote_callback);

	pp::CompletionCallback callback = callback_factory.NewCallback(
		&BIOTunnel::MessageLoopWorker);
	pp::MessageLoop::GetCurrent().PostWork(callback, 100);
}

void BIOTunnel::OnLocalRead(int32_t result) {
	std::ostringstream message;
	if (result <= 0 && result != PP_ERROR_INPROGRESS) {
		if (result == 0) {
			message << "local: client disconnected";
		} else {
			message << "local: Read returned code: " << result;
		}
		instance.PostMessage(message.str());
		Close();
		return;
	}

	if (result != PP_ERROR_INPROGRESS) {
		message << "local: Read " << result << " bytes";
		instance.PostMessage(message.str());
		if (filter_data) {
			int32_t filter_result = FilterData(result);
			if (filter_result != PP_ERROR_INPROGRESS) {
				BIO_write(bio_local, local_filter_buffer, filter_result);
			} else {
				pp::CompletionCallback local_callback =
					callback_factory.NewCallback(&BIOTunnel::OnLocalRead);
				local_socket.Read(local_read_buffer + result,
					BIO_get_write_guarantee(bio_local) - result,
					local_callback);
			}
		} else {
			BIO_write(bio_local, local_read_buffer, result);
		}
	}
}

void BIOTunnel::OnLocalWrite(int32_t result) {
	std::ostringstream message;
	if (result <= 0) {
		if (result == 0) {
			message << "local: client disconnected";
		} else {
			message << "local: Write returned code: " << result;
		}
		instance.PostMessage(message.str());
		Close();
		return;
	}

	message << "local: Wrote " << result << " bytes";
	instance.PostMessage(message.str());

	pp::CompletionCallback remote_callback = callback_factory.NewCallback(
		&BIOTunnel::OnRemoteRead);
	remote_socket.Read(remote_read_buffer, BIO_get_write_guarantee(bio_remote),
		remote_callback);
}

void BIOTunnel::OnRemoteRead(int32_t result) {
	std::ostringstream message;
	if (result <= 0 && result != PP_ERROR_INPROGRESS) {
		if (result == 0) {
			message << "remote: client disconnected";
		} else {
			message << "remote: Read returned code: " << result;
		}
		instance.PostMessage(message.str());
		Close();
		return;
	}

	if (result != PP_ERROR_INPROGRESS) {
		message << "remote: Read " << result << " bytes";
		instance.PostMessage(message.str());
		BIO_write(bio_remote, remote_read_buffer, result);
	}
}

void BIOTunnel::OnRemoteWrite(int32_t result) {
	std::ostringstream message;
	if (result <= 0) {
		if (result == 0) {
			message << "remote: client disconnected";
		} else {
			message << "remote: Write returned code: " << result;
		}
		instance.PostMessage(message.str());
		Close();
		return;
	}

	message << "remote: Wrote " << result << " bytes";
	instance.PostMessage(message.str());

	if (!ctx || ssl_connected) {
		pp::CompletionCallback local_callback = callback_factory.NewCallback(
			&BIOTunnel::OnLocalRead);
		local_socket.Read(local_read_buffer, BIO_get_write_guarantee(bio_local),
			local_callback);
	}
}
