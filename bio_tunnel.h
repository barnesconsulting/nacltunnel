/*
 * bio_tunnel.h
 *
 *  Created on: Apr 25, 2014
 *      Author: scott
 */

#ifndef BIO_TUNNEL_H_
#define BIO_TUNNEL_H_

#include <openssl/ssl.h>
#include <string>
#include <ppapi/cpp/tcp_socket.h>
#include <ppapi/cpp/net_address.h>
#include <ppapi/cpp/host_resolver.h>
#include <ppapi/cpp/completion_callback.h>
#include <ppapi/utility/completion_callback_factory.h>

#define BIO_TUNNEL_BUFFER_SIZE 17408

class BIOTunnel {
public:
	BIOTunnel(pp::Instance &pp_instance);
	~BIOTunnel();

	void Connect(const pp::TCPSocket &new_local_socket,
		const std::string &new_remote_host, bool use_ssl = false,
		bool use_filter = false);
	void Close();

protected:
	void MessageLoopWorker(int32_t status);
	int32_t FilterData(int32_t bytes);

	void OnResolve(int32_t result);
	void OnConnect(int32_t result);

	void OnLocalRead(int32_t result);
	void OnLocalWrite(int32_t result);

	void OnRemoteRead(int32_t result);
	void OnRemoteWrite(int32_t result);

	pp::CompletionCallbackFactory<BIOTunnel> callback_factory;

	pp::Instance &instance;

	pp::HostResolver resolver;
	std::string remote_host;
	pp::NetAddress remote_address;

	pp::TCPSocket local_socket;
	pp::TCPSocket remote_socket;

	char local_read_buffer[BIO_TUNNEL_BUFFER_SIZE];
	char local_filter_buffer[BIO_TUNNEL_BUFFER_SIZE];
	char local_write_buffer[BIO_TUNNEL_BUFFER_SIZE];
	char remote_read_buffer[BIO_TUNNEL_BUFFER_SIZE];
	char remote_write_buffer[BIO_TUNNEL_BUFFER_SIZE];

	BIO *bio_local, *bio_remote, *bio_ssl_local;
	SSL *ssl;
	SSL_CTX *ctx;

	bool ssl_connected;
	bool filter_data;
};

#endif /* BIO_TUNNEL_H_ */
