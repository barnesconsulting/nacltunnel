// Copyright (c) 2013 T. Scott Barnes. Based on the "socket" NaCl example.

// Copyright (c) 2013 The Chromium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

#include <sstream>
#include <netinet/in.h>
#include <ppapi/cpp/var.h>
#include <nacl_io/nacl_io.h>
#include "tunnel_instance.h"
#include "bio_tunnel.h"

#ifdef WIN32
#undef PostMessage
// Allow 'this' in initializer list
#pragma warning(disable : 4355)
#endif

// Number of connections to queue up on the listening
// socket before new ones get "Connection Refused"
static const int kBacklog = 10;

TunnelInstance::TunnelInstance(PP_Instance instance) :
		pp::Instance(instance),
		callback_factory(this),
		remote_host("216.97.58.27:7443"),
		remote_ssl(true),
		use_filter(true) {
}

TunnelInstance::~TunnelInstance() {
}

void TunnelInstance::HandleMessage(const pp::Var& var_message) {
	if (!var_message.is_string())
		return;
	std::string message = var_message.AsString();
	// This message must contain a command character followed by ';' and
	// arguments like "X;arguments".
	if (message.length() < 2 || message[1] != ';')
		return;
	std::ostringstream status;
	switch (message[0]) {
	case MSG_SET_REMOTE_HOST:
		remote_ssl = (message[2] == 's');
		remote_host = message.substr(4);
		status << "Remote host changed to: " << message.substr(2);
		PostMessage(status.str());
		break;
	case MSG_SET_USE_FILTER:
		use_filter = (message[2] == 'y');
		status << "tunnel: ";
		if (!use_filter)
			status << "not ";
		status << "using filter";
		PostMessage(status.str());
		break;
	case MSG_CLOSE:
		// The command 'c' requests to close without any argument like "c;"
		//Close();
		break;
	case MSG_LISTEN: {
		// The command 'l' starts a listening socket (server).
		int port = atoi(message.substr(2).c_str());
		Listen(port);
		break;
	}
	default:
		status << "Unhandled message from JavaScript: " << message;
		PostMessage(status.str());
		break;
	}
}

void TunnelInstance::Listen(uint32_t listening_port) {
	if (!listening_socket.is_null()) {
		listening_socket.Close();
	}
	listening_socket = pp::TCPSocket(this);
	if (listening_socket.is_null()) {
		PostMessage("Error creating TCPSocket.");
		return;
	}

	std::ostringstream status;
	status << "Starting server on port: " << listening_port;
	PostMessage(status.str());

	// Attempt to listen on all interfaces (0.0.0.0)
	// on the given port number.
	PP_NetAddress_IPv4 ipv4_addr = { htons(listening_port), { 0 } };
	pp::NetAddress addr(this, ipv4_addr);
	pp::CompletionCallback callback = callback_factory.NewCallback(
		&TunnelInstance::OnBindCompletion);
	int32_t rtn = listening_socket.Bind(addr, callback);
	if (rtn != PP_OK_COMPLETIONPENDING) {
		PostMessage("Error binding listening socket.");
		return;
	}
}

void TunnelInstance::TryAccept() {
	pp::CompletionCallbackWithOutput<pp::TCPSocket> callback =
		callback_factory.NewCallbackWithOutput(
			&TunnelInstance::OnAcceptCompletion);
	listening_socket.Accept(callback);
}

void TunnelInstance::OnBindCompletion(int32_t result) {
	if (result != PP_OK) {
		std::ostringstream status;
		status << "instance: Bind failed with: " << result;
		PostMessage(status.str());
		return;
	}

	pp::CompletionCallback callback = callback_factory.NewCallback(
		&TunnelInstance::OnListenCompletion);

	int32_t rtn = listening_socket.Listen(kBacklog, callback);
	if (rtn != PP_OK_COMPLETIONPENDING) {
		PostMessage("instance: Error listening on server socket.");
		return;
	}
}

void TunnelInstance::OnListenCompletion(int32_t result) {
	std::ostringstream status;
	if (result != PP_OK) {
		status << "instance: Listen failed with: " << result;
		PostMessage(status.str());
		return;
	}

	pp::NetAddress addr = listening_socket.GetLocalAddress();
	status << "instance: Listening on: "
		<< addr.DescribeAsString(true).AsString();
	PostMessage(status.str());

	TryAccept();
}

void TunnelInstance::OnAcceptCompletion(int32_t result, pp::TCPSocket socket) {
	std::ostringstream status;

	if (result != PP_OK) {
		status << "instance: Accept failed: " << result;
		PostMessage(status.str());
		return;
	}

	pp::NetAddress addr = socket.GetLocalAddress();
	status << "instance: New connection from: ";
	status << addr.DescribeAsString(true).AsString();
	PostMessage(status.str());
	BIOTunnel* new_tunnel = new BIOTunnel(*this);
	new_tunnel->Connect(socket, remote_host, remote_ssl, use_filter);

	TryAccept();
}
