#ifndef TUNNEL_INSTANCE_H_
#define TUNNEL_INSTANCE_H_

#include <string>
#include <vector>

#include "ppapi/cpp/instance.h"
#include "ppapi/cpp/tcp_socket.h"
#include "ppapi/utility/completion_callback_factory.h"
#include "bio_tunnel.h"

class TunnelInstance: public pp::Instance {
public:
	explicit TunnelInstance(PP_Instance instance);

	virtual ~TunnelInstance();

	virtual void HandleMessage(const pp::Var& var_message);

	void Listen(uint32_t listening_port);

private:
	void TryAccept();

	// Callback functions - listening
	void OnBindCompletion(int32_t result);
	void OnListenCompletion(int32_t result);
	void OnAcceptCompletion(int32_t result, pp::TCPSocket socket);

	pp::CompletionCallbackFactory<TunnelInstance> callback_factory;

	pp::TCPSocket listening_socket;

	std::string remote_host;
	bool remote_ssl;
	bool use_filter;
};

#define MSG_SET_REMOTE_HOST 'h'
#define MSG_SET_USE_FILTER 'f'
#define MSG_CLOSE 'c'
#define MSG_LISTEN 'l'

#endif /* TUNNEL_INSTANCE_H_ */
